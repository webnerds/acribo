<?php 

// Template Name: Start

 ?>

<?php while (have_posts()) : the_post(); ?>

	<div class="banner" style="background-image: url('<?php the_field('banner-bg');?>')">
		<div class="row">
			<div class="columns large-7">
				<div class="video flex-video widescreen">
					<iframe width="853" height="480" src="<?php the_field('movie_link'); ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="columns large-5">
				<div class="banner-text">
					<?php the_field('banner_text') ?>
				</div>
			</div>
		</div>
	</div>

	<div class="register-interest">
		<div class="row">
			<div class="columns large-10 large-centered">
				<div class="columns">
					<?php the_field('register_interest_text');?>
				</div>
				<?php echo do_shortcode(get_field('register_interest_form')); ?>	
			</div>
		</div>
	</div>

	<div class="the-project" id="byggprojektet">
		<div class="row">
			<div class="columns large-10 large-centered">
				<?php 
				$content = explode('<!--two-column-start-->', get_field('project_text')); 
				?>
				<?= $content[0] ?>
				<div class="two-columns">
					<?= wpautop($content[1]); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="appartments" id="lagenheter">
		<div class="row">
			<div class="columns large-8 large-centered">
				<?php the_field('appartments_text') ?>
			</div>
		</div>
		<div class="appartment-types">
		<?php 
			if( have_rows('appartments') ):
				$i = 0;
			    while ( have_rows('appartments') ) : the_row();
			    	$i++;
			    ?>
					 <div class="appartment">
						<div class="row">
							<div class="image-container" style="background-image: url('<?php the_sub_field('main_image');?>')">
							</div>
							<div class="columns large-6 <?php if($i%2 == 0) echo 'end' ?>">
								<div class="description">
									<?php the_sub_field('description'); ?>
									<?php 
									$images = get_sub_field('images');

									if( $images ): ?>
									    <ul class="images">
									        <?php foreach( $images as $image ): ?>
									            <li>
									                <a data-title="<?php echo $image['caption']; ?>" data-lightbox="images-<?php echo $i;?>" href="<?php echo $image['url']; ?>">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            </li>
									        <?php endforeach; ?>
									    </ul>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
			<?php endwhile;
		    endif;?>
	    </div>
	</div>

	<div class="example-project" id="exempel">
		<div class="row">
			<div class="columns large-8 large-centered">
				<?php the_field('example_project_text') ?>
			</div>
		</div>
	</div>

	<div class="about" id="om-acribo">
		<div class="row">
			<div class="columns large-8 large-centered">
				<?php the_field('about_text') ?>
			</div>
		</div>
	</div>

	<div class="contact" id="kontakt">
		<div class="row">
			<div class="columns large-8 large-centered">
				<?php the_field('contact_text') ?>
			</div>
		</div>
	</div>
	
	<div class="partners" id="partners">
		<div class="row">
			<div class="columns large-12 large-centered">
				<?php the_field('partners_text') ?>
				<?php 
			if( have_rows('partners') ):
				$i = 0;
			    while ( have_rows('partners') ) : the_row();?>
				
				<img class="partner-logo" src="<?php the_sub_field('logo'); ?>" alt="">
		
			    <?php endwhile;
		    endif;?>
			</div>
		</div>
	</div>

<?php endwhile; ?>
