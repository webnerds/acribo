/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        $(document).foundation(); // Foundation JavaScript
        
        $(window).scroll(function() {
          if ($(document).scrollTop() > 600) {
            $(".site-header").addClass("sticky");
          }
          else {
            $(".site-header").removeClass("sticky");
          }
        });

        $('.site-header .mobile-nav-toggle').click(function(){
          $('body').toggleClass('mobile-nav-open');
        });

        $(window).resize(function() {
          $('body').removeClass('mobile-nav-open');
        });

        $(window).click(function(){
          $(".mobile-sidebar").hide();
        });

        $(".mobile-sidebar-toggle").click(function(e) {
          e.stopPropagation();
            if ($(".mobile-sidebar").css("display") === "none") {
                $(".mobile-sidebar").show();
            } else {
                $(".mobile-sidebar").hide();
            }
        });

        lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true,
          'albumLabel': "Bild %1 av %2",
          'positionFromTop': 50,
          'disableScrolling': true
        });

        $('.menu-item a').click(function(e) {
          $('body').removeClass('mobile-nav-open');
          $('html,body').animate({
              scrollTop: $($(this).prop("hash")).offset().top
            }, 1000);
        });

        $(".site-header .menu-item-has-children").hover(
          function () {
              $(".sub-menu").show();
          },
          function () {
              $(".sub-menu").hide();
          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },

    'page_template_start' : {
      init: function() {
        $("#banner-button").click(function() {
          $('html, body').animate({
              scrollTop: $(".contact").offset().top - 200
          }, 1000);
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
