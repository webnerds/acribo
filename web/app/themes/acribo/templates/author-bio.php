<?php 
$author_img = get_avatar( get_the_author_meta('email'), 200 );
 ?>

<div class="bio-content">
	<?= get_avatar( get_the_author_meta('email'), 150 ); ?>
  <div class="bio-text">
    <h4>Av <?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?> för <?= human_time_diff(get_the_date('U')) ?> sedan</h4>
    <p><?php the_author_meta('description'); ?></p>
  </div>
</div>