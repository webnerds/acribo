<div class="single-blog-post">

<?php while (have_posts()) : the_post(); ?>
  
  <article <?php post_class(); ?>>
    <div class="row">
      <div class="medium-10 large-8 medium-centered columns text-center">
      <p>Postat för <?= human_time_diff(get_the_date('U')) ?> sedan</p>
        <header>
          <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <div class="entry-content text-left">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </article>

  <div class="row collapse">
    <div class="medium-10 large-8 columns medium-centered">
      <?php get_template_part('templates/author-bio')?>
    </div>
  </div>

<?php endwhile; ?>

</div>
