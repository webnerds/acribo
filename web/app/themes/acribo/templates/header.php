<?php 
$site_header_classes = ""; 
$logo = get_field('dark_logo', 'option');
$light_logo = get_field('light_logo', 'option');


if(get_field('show_banner') && get_field('transparent_header')) {
  $site_header_classes .= "dark-bg ";
  $logo = get_field('light_logo', 'option');
}

//Check if menu toggle should show
$menu_toggle_class ="";
if ( !has_nav_menu( 'mobile_menu' ) && !has_nav_menu( 'expanded_menu' )) {
  $menu_toggle_class = "hide";
}elseif( !has_nav_menu( 'expanded_menu' ) ) {
  $menu_toggle_class = "hide-for-large";
}elseif(!has_nav_menu( 'mobile_menu' )){
  $menu_toggle_class = "show-for-large";
}

?>
<header class="site-header <?= $site_header_classes ?>">
 
    <div class="row">
      <div class="small-6 medium-3 columns">
          <h3 class="menu-logo">
            <a class="logo" href="<?php bloginfo('url') ?>" style="background-image: url('<?= $logo ?>');"><?php bloginfo('name') ?></a>
            <a class="logo-light" href="<?php bloginfo('url') ?>" style="background-image: url('<?php echo $light_logo; ?>');"><?php bloginfo('name') ?></a>
          </h3>
      </div>   
      <div class="small-6 medium-9 columns">
        <div class="hide-for-large action-nav">
          <ul>
          <?php if(get_field('header_has_cta', 'option')): ?>
            <li><a href="<?php the_field('header_cta_link', 'option') ?>" class="button"><?php the_field('header_cta_title', 'option') ?></a></li>
          <?php endif; ?>
          <li>
            &nbsp;
          </li>
          <li>
            <div class="mobile-nav-toggle <?= $menu_toggle_class ?>">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </li>
          </ul>
        </div>
        <div class="show-for-large">
          <div class="web-menu">
            <ul>
              <?php wp_nav_menu( array( 
                'theme_location' => 'quick_menu',
                'container' => '',
                'items_wrap'    => '%3$s',
                'depth'=> 2
              ) ); ?> 
              <?php if(get_field('header_has_cta', 'option')): ?>
                <li><a href="<?php the_field('header_cta_link', 'option') ?>" class="button"><?php the_field('header_cta_title', 'option') ?></a></li>
              <?php endif; ?>
                <li class="mobile-nav-toggle <?= $menu_toggle_class ?>">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</header>

<div id="mobile-nav">
  <div class="row collapse">
    <div class="columns">
      <ul>
        <?php wp_nav_menu( array( 
          'theme_location' => 'mobile_menu',
          'container' => '',
          'items_wrap'    => '%3$s',

        ) ); ?>
      </ul>
    </div>
  </div>
</div>