<?php 
$facebook = get_field('facebook', 'option');
$instagram = get_field('instagram', 'option');
$youtube = get_field('youtube', 'option');
 ?>

<footer class="site-footer">
<?php /*
  <!-- Banner -->
  <div class="footer-banner">
   	<div class="row collapse">
   		<div class="columns text-center">	
			<p class="banner-text">Ha inte ont i onödan, vi hjälper dig!</p>
			<a href="<?php the_field('booking_link', 'option') ?>" class="button">Boka tid</a>
   		</div>	
   	</div>
  </div>
  
  <!-- Content -->
  <div class="footer-content">
		<div class="footer-columns">
  		<div class="row">	
	  		<div class="large-3 medium-6 columns">
	  			<h3>Kontakt</h3>
	  			<?php the_field('contact_info', 'option'); ?>
	  		</div>
	  		<div class="large-3 show-for-large columns">
	  			<h3>Sidor</h3>
	  			<?php wp_nav_menu( array( 
		        'theme_location' => 'primary_navigation',
		        'container' => '',
		        'depth' => 1
		      ) ); ?>
	  		</div>	
	  		<div class="large-3 show-for-large columns">
	  			<h3>Nyheter</h3>
	  			<ul>
				<?php
					$recent_posts = wp_get_recent_posts(array(
						'numberposts' => 5,
					));
					foreach( $recent_posts as $recent ){
						echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
					}
				?>
				</ul>
  				<a href="<?= get_permalink( get_option( 'page_for_posts' ) ); ?>">Alla nyheter</a>
	  		</div>
	  		<div class="large-3 medium-6 columns">
	  			<div class="social">
	  				<h3>Socialt</h3>
	  				<?php if(!empty($facebook)): ?>
	  					<a href="<?= $facebook ?>"><i class="fa fa-facebook-square"></i></a>
	  				<?php endif; 
	  				if(!empty($instagram)): ?>
	  					<a href="<?= $instagram ?>"><i class="fa fa-instagram-square"></i></a>
	  				<?php endif;
	  				if(!empty($youtube)): ?>
	  					<a href="<?= $youtube ?>"><i class="fa fa-youtube-square"></i></a>
	  				<?php endif; ?>
	  			</div>
	  		</div>
	  	</div>
	  </div>
  </div>

  <!-- Bottom Ribbon -->
  <div class="bottom-ribbon">
  	<div class="row">
  		<div class="medium-3 columns">
  			<h1>
  				<a class="bottom-ribbon-logo" href="<?php bloginfo('url') ?>">Lidingonaprapaterna</a>
  			</h1>
  		</div>
  		<div class="medium-5 medium-text-right columns">
  			<p><small>Copyright 2016 | <a href="<?php the_field('site_info', 'option'); ?>">Sidinformation</a></small></p>
  		</div>
  	</div>
  </div>
*/?>
</footer>
